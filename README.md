DEPRECATED ---- LDAPI Development Environment
=============================================

**From Ldapi v1.0.0 unit testing is applied, which should be the desired method of testing and development!**

**These unit tests can be found inside the Ldapi repository itself.**

**This repo is now cleared and could be deleted.**
